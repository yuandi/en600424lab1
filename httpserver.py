#!/usr/bin/env python

# Copyright (c) Twisted Matrix Laboratories.
# See LICENSE for details.
from __future__ import print_function

from twisted.internet.protocol import Protocol, Factory
from twisted.internet import reactor
from twisted.internet.endpoints import TCP4ServerEndpoint
from twisted.protocols.basic import LineReceiver

import re
import os
import time

### Protocol Implementation

RootPath = "/home/ydx/Desktop/dev/HTTPRoot"
# This is just about the simplest possible protocol
class Http(Protocol):
    def dataReceived(self, data):
		MatchGET = re.search(r'\bGET\b.*\bHTTP/1.[10]\b', data, re.M)
		if MatchGET:
			print("GETDATA:", MatchGET.group())
			MatchHttp = re.search(r'\bHTTP/1.[10]\b', data, re.M)
			FileNameEndPos = MatchHttp.start() - 1
			FileName = data[MatchGET.start()+4:FileNameEndPos]
			FileName = RootPath + FileName
			str_send = data[FileNameEndPos + 1 : FileNameEndPos + 9];
			if os.path.exists(FileName):
				if os.path.isfile(FileName):
			#		print("i'm a file")
					FileObj = open(FileName, "r")
					FileStr = FileObj.read() + "\r\n"
					FileObj.close()
					str_send += " 200 OK\r\n"
					date = time.strftime(u"%a, %d %b %Y %H:%M:%S GMT\r\n\r\n", time.gmtime())
					str_send = str_send + date
					str_send = str_send + FileStr + "\r\n\r\n"
				else:
					FileName += "/index.html"
					if os.path.exists(FileName):
				#		print("i'm a dir and have an index")
						FileObj = open(FileName, "r")
						FileStr = FileObj.read() + "\r\n"
						FileObj.close()
						str_send += " 200 OK\r\n"
						date = time.strftime(u"%a, %d %b %Y %H:%M:%S GMT\r\n\r\n", time.gmtime())
						str_send = str_send + date
						str_send = str_send + FileStr + "\r\n\r\n"
					else:
				#		print("i'm a dir and have no index")
						str_send += " 404 Not Found\r\n"
						date = time.strftime(u"%a, %d %b %Y %H:%M:%S GMT\r\n\r\n", time.gmtime())
						str_send += date
			else:
				str_send += " 404 Not Found\r\n"
				date = time.strftime(u"%a, %d %b %Y %H:%M:%S GMT\r\n\r\n", time.gmtime())
				str_send += date
			self.transport.write(str_send)
		else:
			str_send = data[FileNameEndPos + 1 : FileNameEndPos + 9];
			str_send += (" 403 Bad Request\r\n")
			date = time.strftime(u"%a, %d %b %Y %H:%M:%S GMT\r\n\r\n", time.gmtime())
			str_send += date
			self.transport.write(str_send)


class ServerFactory(Factory):
	def buildProtocol(self, addr):
		return Http()

def main():
	endpoint = TCP4ServerEndpoint(reactor, 80)
	endpoint.listen(ServerFactory())
	reactor.run()

if __name__ == '__main__':
    main()
