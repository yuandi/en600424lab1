
from __future__ import print_function

from twisted.internet import task
from twisted.internet.defer import Deferred
from twisted.internet.protocol import ClientFactory
from twisted.protocols.basic import LineReceiver
from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.internet import reactor



class HttpClient(LineReceiver):

    def connectionMade(self):
	tempstr = raw_input("Input reuqested path: ")
        self.sendLine("GET "+tempstr+ " HTTP/1.0\r\n")


    def lineReceived(self, line):
	if (line != "\r\n"):
        	print(line)


class HttpClientFactory(ClientFactory):
    protocol = HttpClient

    def __init__(self):
        self.done = Deferred()


    def clientConnectionFailed(self, connector, reason):
        print('connection failed:', reason.getErrorMessage())
        self.done.errback(reason)


    def clientConnectionLost(self, connector, reason):
        print('connection lost:', reason.getErrorMessage())
        self.done.callback(None)



def main():
#    factory = HttpClientFactory()
#    reactor.connectTCP('localhost', 80, factory)
#    return factory.done
	endpoint = TCP4ClientEndpoint(reactor, "localhost", 80)
	endpoint.connect(HttpClientFactory())
	reactor.run()

if __name__ == '__main__':
    main()
